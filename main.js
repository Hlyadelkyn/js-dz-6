"use strict";
//ForEach выполняет функцию для каждого элемента массива

function filterBy(arr, dataType) {
  let result = arr.filter((item) => !(typeof item === dataType));
  return result;
}

let arr1 = ["cat", 1, true, "whool", "school", "", 1332];
let type = "string";

console.log(filterBy(arr1, type));
